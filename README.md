# README #

##### changelog #####

Last updated 07/December/2016


ver 0.13 

* Fixed unable to load subreddits due to change in reddit HTML. Had to change Regex to accomodate that change.

ver 0.12 

* Fixed UpdateRate Option in Stream measure from 300 to 900 to actually be 15 minutes interval. 300 was updating every 5 mintues.

ver 0.11

* Added ability to change subreddit by clicking on the subreddit name
* Added displaying external link url when hovering on score.
* Removed external link icon from popping on right - makes more sense now positioning the skin to the right edge of screen
* Added error message when RegExp fails on subbreddit fetching - means no such subreddits
* Changed "Substitute" function for DecodeCharacterReference=1 on titles.
* Variable renaming for clarity


### redditReader ###

* Very basic Reddit reader skin for Rainmeter. Uses WebParser to read from subreddit front page. Comes in two variants: 8 rows and 16 rows.

Preview screenshots: http://copperstain.deviantart.com/art/redditReader-KotakuInAction-flavored-605441985

### How to use ###

1. Copy the copperStain folder under your Rainmeter skins folder. On Win7, it's C:\Users\\[username]\Documents\Rainmeter\Skins\

2. Get the .rmskin file from http://copperstain.deviantart.com/art/redditReader-KotakuInAction-flavored-605441985 and install from there.

### Dependencies/Compatibility ###

* Tested on Windows 7 Ultimate 64-bit (build 7601) Service Pack 1
* Rainmeter version 3.2.1 r2386 64-bit (Mar 24 2015)
* No third-party plugin dependencies. Uses inbuilt WebParser plugin.

### Author ###

* rightdroid@gmail.com
* https://twitter.com/rightdroid